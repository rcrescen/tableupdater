from flask import Flask, send_file, send_from_directory, render_template
from flask_restful import reqparse, abort, Api, Resource
import io
import time

from db import DB

app = Flask(__name__)
api = Api(app)

myDB=DB()

def force_abort():
  abort(404, message="Resource doesn't exist")

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/static/<path:path>')
def send_js(path):
  return send_from_directory('static', path)

@app.route('/admin')
def admin():
  return render_template('trunk.html')

@api.resource('/getstatus')
class GetStatus(Resource):
  def get(self):
    return myDB.get_status()

@api.resource('/trunk')
class Trunk(Resource):
  def get(self):
    return myDB.trunk()

@api.resource('/update/<int:col>/<int:row>/<int:status>')
class ConvertDefault(Resource):
  def get(self, col, row, status):
    return myDB.update(str(col), str(row), str(status))
    




if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True)