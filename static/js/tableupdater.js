var h_dict = ['Seleccione', 'Bajo', 'Medio', 'Alto'];
var max_score = h_dict.length - 1;
var c_dict = ['', '#ff9999', '#ffff99', '#66b266'];
var ce_dict = ['', '#ff9999', '#ffa98f', '#ffbb88', '#ffcf86', '#ffe38b', '#f3e688', '#e6e988', '#d8ec89', '#bcde7e', '#9fcf75', '#83c16d', '#66b266'];
var ce_offset = 3;

var lvl_sizes = [14, 21, 3, 1, 3, 3, 3, 1, 1, 1, 1, 2, 8, 10, 6, 3, 12, 4, 3, 5, 1, 2];

$(document).ready(function () {


  var table_ejecutivo=  $('#table-ejecutivo').prop('outerHTML');
  var table_general=  $('#table-general').prop('outerHTML');

  $.ajax('getstatus').done(function (data) {
    fill_ejecutivo(data, table_ejecutivo);
    fill_general(data, table_general);
  });

  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

    $.ajax('getstatus').done(function (data) {
      fill_ejecutivo(data, table_ejecutivo);
      fill_general(data, table_general);
    });
  });

});

function fill_ejecutivo(data, table_ejecutivo) {
  var table_fresh=$(table_ejecutivo);

  $('#table-ejecutivo').replaceWith(table_fresh);

    resp = eval(data);
    let count_cols = resp[0].length - 1;
    let lvl = 0;
    let lvl_poper = 0;
    let arr_sum = [];

    for (var i = 0; i < count_cols; i++) {
      arr_sum.push(0);
    }

    resp.forEach(element => {
      let idx = element[0];
      let td = ''


      for (var t = 1; t < element.length; t++) {
        arr_sum[t - 1] += element[t];
      }
      lvl_poper++;
      if (lvl_poper == lvl_sizes[lvl]) {
        let cell = $('#e' + (lvl + ce_offset));
        for (var i = 0; i < count_cols; i++) {
          let local_sum = Math.ceil(100 * arr_sum[i] / (lvl_sizes[lvl] * max_score));
          let color = Math.ceil((local_sum * (ce_dict.length - 1)) / 100);
          arr_sum[i] = 0;
          td += '<td class="text-center" style="background-color:' + ce_dict[color] + '"><div class="align-middle">' + local_sum + '%</div></td>';
        }
        cell.append(td);
        lvl++;
        lvl_poper = 0;
      }
  });
}

function fill_general(data, table_general) {
  var table_fresh=$(table_general);
    
  $('#table-general').replaceWith(table_fresh);

    resp = eval(data);
    resp.forEach(element => {
      let idx = element[0];
      let cell = $('#c' + idx);

      for (var i = 1; i < element.length; i++) {
        let td = '<td id="d' + i + '-' + idx + '" class="select"><select id="s' + i + '-' + idx + '" class="form-control-sm" data-col="' + i + '" data-row="' + idx + '">';
        for (var t = 0; t < h_dict.length; t++) {
          let selected = '';
          if (element[i] == t) {
            selected = ' selected="selected"'
          }
          td = td + '<option' + selected + ' value="' + t + '">' + h_dict[t] + '</option>';
        }
        cell.append(td + '</select></td>');

        $('#s' + i + '-' + idx).css('background-color', c_dict[element[i]]);
      }
    });

    $("select").change(function () {
      that = $(this);
      that.css('display', 'none');

      var wait_spin = $('<button class="btn btn-primary btn-sm"  type="button" disabled>' +
        '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' +
        '<span style="font-size: 0.7rem">Actualizando<span>' +
        '</button>');

      var id_str = '#d' + $(this).data('col') + '-' + $(this).data('row');
      $(id_str).append(wait_spin);

      $.ajax('update/' + $(this).data('col') + '/' + $(this).data('row') + '/' + that[0].value).done(function (data) {

        switch (that[0].value) {
          case "0":
            that.css("background-color", c_dict[0]);
            break;
          case "1":
            that.css("background-color", c_dict[1]);
            break;
          case "2":
            that.css("background-color", c_dict[2]);
            break;
          case "3":
            that.css("background-color", c_dict[3]);
            break;
          default:
            that.css("background-color", "");
        }
        that.css('display', 'block');
        wait_spin.remove();
      });

    });
}