from setuptools import find_packages, setup

setup(
    name='pyfileharvester',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
      'Flask==1.1.2',
      'Flask_RESTful==0.3.8'
    ]
)