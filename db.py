import json
import sqlite3

class DB():
  connection=None
  
  def __init__(self):
    self.conn=None

  def get_status(self):
    with sqlite3.connect('status.db') as conn:
      cur = conn.cursor()
      cur.execute("SELECT * from status order by id ")
      data = cur.fetchall()
      return json.dumps(data)
    return 0

  def update(self, col, row, status):
    with sqlite3.connect('status.db') as conn:
      cur = conn.cursor()
      update_str="UPDATE status SET \""+col+"\"="+status+" WHERE id="+row
      cur.execute(update_str)
      conn.commit()
      if(cur.rowcount > 0):
        return json.dumps({"status":"success", "value":status})

  def trunk(self):
    with sqlite3.connect('status.db') as conn:
      cur = conn.cursor()
      update_str="UPDATE status set \"1\"=0,  \"2\"=0, \"3\"=0, \"4\"=0, \"5\"=0, \"6\"=0"
      cur.execute(update_str)
      conn.commit()
      print(cur.rowcount);
      if(cur.rowcount == 10 ):
        return json.dumps({"status":"success", "value":str(cur.rowcount)})

    return 0